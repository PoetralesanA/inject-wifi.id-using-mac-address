﻿Public Class eight
    Public Shared Function DIE()
        Shell("netsh interface set interface ""wi-fi 2"" DISABLE", False)
        Shell("netsh interface set interface ""wi-fi"" DISABLE", False)
        Shell("netsh interface set interface ""wireless network connection"" DISABLE", False)
        Shell("netsh interface set interface ""wireless network connection 2"" DISABLE", False)
    End Function
    Public Shared Function LIFE()
        Shell("netsh interface set interface ""wireless network connection 2"" ENABLED", False)
        Shell("netsh interface set interface ""wireless network connection"" ENABLED", False)
        Shell("netsh interface set interface ""wi-fi"" ENABLED", False)
        Shell("netsh interface set interface ""wi-fi 2"" ENABLED", False)
    End Function
End Class
