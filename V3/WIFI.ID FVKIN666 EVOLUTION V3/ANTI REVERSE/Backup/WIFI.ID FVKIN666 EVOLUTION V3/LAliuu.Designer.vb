﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LAliuu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LAliuu))
        Me.masteng = New WIFI.ID_FVKIN666_EVOLUTION_V3.FormSkin()
        Me.calek = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox()
        Me.bstrad = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatGroupBox()
        Me.momo = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox()
        Me.sincan = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox()
        Me.lutung = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaControlBox()
        Me.awasButaKaka = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton()
        Me.masterrr = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox()
        Me.GilaLuNro = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatGroupBox()
        Me.bhaks = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox()
        Me.masteng.SuspendLayout()
        Me.bstrad.SuspendLayout()
        Me.GilaLuNro.SuspendLayout()
        Me.SuspendLayout()
        '
        'masteng
        '
        Me.masteng.BackColor = System.Drawing.Color.White
        Me.masteng.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.masteng.BorderColor = System.Drawing.Color.FromArgb(CType(CType(53, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.masteng.Controls.Add(Me.calek)
        Me.masteng.Controls.Add(Me.bstrad)
        Me.masteng.Controls.Add(Me.lutung)
        Me.masteng.Controls.Add(Me.awasButaKaka)
        Me.masteng.Controls.Add(Me.masterrr)
        Me.masteng.Controls.Add(Me.GilaLuNro)
        Me.masteng.Dock = System.Windows.Forms.DockStyle.Fill
        Me.masteng.FlatColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.masteng.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.masteng.HeaderColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.masteng.HeaderMaximize = False
        Me.masteng.Location = New System.Drawing.Point(0, 0)
        Me.masteng.Name = "masteng"
        Me.masteng.Size = New System.Drawing.Size(450, 412)
        Me.masteng.TabIndex = 0
        Me.masteng.Text = "                                R E G I S T E R"
        '
        'calek
        '
        Me.calek.BackColor = System.Drawing.Color.Transparent
        Me.calek.Enabled = False
        Me.calek.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.calek.Location = New System.Drawing.Point(-4, 384)
        Me.calek.MaxLength = 32767
        Me.calek.Multiline = False
        Me.calek.Name = "calek"
        Me.calek.ReadOnly = False
        Me.calek.Size = New System.Drawing.Size(459, 29)
        Me.calek.TabIndex = 0
        Me.calek.Tag = ""
        Me.calek.Text = "© PoetralesanA - 2018 | Cyber Security Down Team"
        Me.calek.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.calek.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.calek.UseSystemPasswordChar = False
        '
        'bstrad
        '
        Me.bstrad.BackColor = System.Drawing.Color.Transparent
        Me.bstrad.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.bstrad.Controls.Add(Me.momo)
        Me.bstrad.Controls.Add(Me.sincan)
        Me.bstrad.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bstrad.Location = New System.Drawing.Point(3, 134)
        Me.bstrad.Name = "bstrad"
        Me.bstrad.ShowText = True
        Me.bstrad.Size = New System.Drawing.Size(447, 169)
        Me.bstrad.TabIndex = 0
        Me.bstrad.Text = "KEY REGISTER"
        '
        'momo
        '
        Me.momo.BackColor = System.Drawing.Color.Transparent
        Me.momo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.momo.Location = New System.Drawing.Point(18, 38)
        Me.momo.MaxLength = 32767
        Me.momo.Multiline = True
        Me.momo.Name = "momo"
        Me.momo.ReadOnly = False
        Me.momo.Size = New System.Drawing.Size(404, 118)
        Me.momo.TabIndex = 2
        Me.momo.TabStop = False
        Me.momo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.momo.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.momo.UseSystemPasswordChar = False
        '
        'sincan
        '
        Me.sincan.BackColor = System.Drawing.Color.Transparent
        Me.sincan.Location = New System.Drawing.Point(237, 38)
        Me.sincan.MaxLength = 32767
        Me.sincan.Multiline = False
        Me.sincan.Name = "sincan"
        Me.sincan.ReadOnly = False
        Me.sincan.Size = New System.Drawing.Size(185, 29)
        Me.sincan.TabIndex = 2
        Me.sincan.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.sincan.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.sincan.UseSystemPasswordChar = False
        Me.sincan.Visible = False
        '
        'lutung
        '
        Me.lutung.BackColor = System.Drawing.Color.Transparent
        Me.lutung.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lutung.ForeColor = System.Drawing.Color.FromArgb(CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.lutung.Location = New System.Drawing.Point(376, 0)
        Me.lutung.Name = "lutung"
        Me.lutung.Size = New System.Drawing.Size(50, 45)
        Me.lutung.TabIndex = 5
        Me.lutung.Text = "SimplaControlBox1"
        '
        'awasButaKaka
        '
        Me.awasButaKaka.BackColor = System.Drawing.Color.WhiteSmoke
        Me.awasButaKaka.BaseColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.awasButaKaka.Cursor = System.Windows.Forms.Cursors.Hand
        Me.awasButaKaka.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.awasButaKaka.Location = New System.Drawing.Point(21, 309)
        Me.awasButaKaka.Name = "awasButaKaka"
        Me.awasButaKaka.Rounded = False
        Me.awasButaKaka.Size = New System.Drawing.Size(404, 43)
        Me.awasButaKaka.TabIndex = 3
        Me.awasButaKaka.TabStop = False
        Me.awasButaKaka.Text = "Login"
        Me.awasButaKaka.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'masterrr
        '
        Me.masterrr.BackColor = System.Drawing.Color.Transparent
        Me.masterrr.Location = New System.Drawing.Point(21, 328)
        Me.masterrr.MaxLength = 32767
        Me.masterrr.Multiline = False
        Me.masterrr.Name = "masterrr"
        Me.masterrr.ReadOnly = False
        Me.masterrr.Size = New System.Drawing.Size(405, 29)
        Me.masterrr.TabIndex = 0
        Me.masterrr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.masterrr.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.masterrr.UseSystemPasswordChar = False
        '
        'GilaLuNro
        '
        Me.GilaLuNro.BackColor = System.Drawing.Color.Transparent
        Me.GilaLuNro.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.GilaLuNro.Controls.Add(Me.bhaks)
        Me.GilaLuNro.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GilaLuNro.Location = New System.Drawing.Point(1, 63)
        Me.GilaLuNro.Name = "GilaLuNro"
        Me.GilaLuNro.ShowText = True
        Me.GilaLuNro.Size = New System.Drawing.Size(447, 93)
        Me.GilaLuNro.TabIndex = 0
        Me.GilaLuNro.Text = "HWID"
        '
        'bhaks
        '
        Me.bhaks.BackColor = System.Drawing.Color.Transparent
        Me.bhaks.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bhaks.Location = New System.Drawing.Point(18, 36)
        Me.bhaks.MaxLength = 32767
        Me.bhaks.Multiline = False
        Me.bhaks.Name = "bhaks"
        Me.bhaks.ReadOnly = True
        Me.bhaks.Size = New System.Drawing.Size(406, 29)
        Me.bhaks.TabIndex = 1
        Me.bhaks.TabStop = False
        Me.bhaks.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.bhaks.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.bhaks.UseSystemPasswordChar = False
        '
        'LAliuu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 412)
        Me.Controls.Add(Me.masteng)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "LAliuu"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "HWID REGISTER"
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.masteng.ResumeLayout(False)
        Me.bstrad.ResumeLayout(False)
        Me.GilaLuNro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents awasButaKaka As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton
    Friend WithEvents bstrad As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatGroupBox
    Friend WithEvents momo As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox
    Friend WithEvents masteng As WIFI.ID_FVKIN666_EVOLUTION_V3.FormSkin
    Friend WithEvents masterrr As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox
    Friend WithEvents lutung As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaControlBox
    Friend WithEvents calek As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox
    Friend WithEvents sincan As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox
    Friend WithEvents GilaLuNro As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatGroupBox
    Friend WithEvents bhaks As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox
End Class
