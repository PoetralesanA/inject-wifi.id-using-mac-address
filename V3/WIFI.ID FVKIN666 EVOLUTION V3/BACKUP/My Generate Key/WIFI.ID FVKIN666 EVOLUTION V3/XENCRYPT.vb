﻿Imports System.Text
Imports System.Security.Cryptography
Public Class XENCRYPT
    Public Shared Function SHA512(ByVal X As String)
        Dim a() As Byte = Encoding.UTF8.GetBytes(X)
        Dim b As Byte()

        Dim c As New SHA512Managed
        b = c.ComputeHash(a)

        Dim d As String = Convert.ToBase64String(b)

        Return d
    End Function
End Class
