﻿Public Class RestrBtnPrgrs

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        HideLoading.Text = HideLoading.Text + 1

        '--------- Set Loading --------- '
        LoadingCircleX.Active = True
        LoadingCircleX.RotationSpeed = 13
        LoadingCircleX.Color = Color.GreenYellow
        LoadingCircleX.InnerCircleRadius = 28
        '------------------------------- '

        If HideLoading.Text = "98" Then
            LoadingCircleX.Active = False
        ElseIf HideLoading.Text = "100" Then
            Timer1.Stop()
            ANDA_MALING.Show()
            Me.Close()
        End If

    End Sub

    Private Sub RestrBtnPrgrs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer1.Start()
       
    End Sub
End Class