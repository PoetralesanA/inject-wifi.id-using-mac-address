﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ANDA_MALING
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ANDA_MALING))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.CheckCon = New System.Windows.Forms.Timer(Me.components)
        Me.DirectKeAnu = New System.Windows.Forms.Timer(Me.components)
        Me.jampe_jampe = New System.Windows.Forms.WebBrowser()
        Me.SimplaTheme1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaTheme()
        Me.ValidateNET = New System.Windows.Forms.Label()
        Me.listAnu = New System.Windows.Forms.ListBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btn_Update = New System.Windows.Forms.Button()
        Me.Xmninim = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton()
        Me.Cls = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton()
        Me.xL = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.AUTOCONNECT = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaCheckBox()
        Me.SimplaProgressBar1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaProgressBar()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RB_SERVER15 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER10 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER5 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER14 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER9 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER4 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER13 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER8 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER3 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER12 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER7 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER2 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER11 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER6 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.StatusConn = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.SimplaButton1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton()
        Me.SimplaButton2 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton()
        Me.SimplaTheme1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Interval = 125
        '
        'BackgroundWorker1
        '
        '
        'CheckCon
        '
        Me.CheckCon.Interval = 4
        '
        'DirectKeAnu
        '
        Me.DirectKeAnu.Interval = 1
        '
        'jampe_jampe
        '
        Me.jampe_jampe.Location = New System.Drawing.Point(0, 0)
        Me.jampe_jampe.MinimumSize = New System.Drawing.Size(20, 20)
        Me.jampe_jampe.Name = "jampe_jampe"
        Me.jampe_jampe.ScriptErrorsSuppressed = True
        Me.jampe_jampe.Size = New System.Drawing.Size(419, 301)
        Me.jampe_jampe.TabIndex = 5
        Me.jampe_jampe.Url = New System.Uri("", System.UriKind.Relative)
        Me.jampe_jampe.Visible = False
        '
        'SimplaTheme1
        '
        Me.SimplaTheme1.AllowDrop = True
        Me.SimplaTheme1.BackColor = System.Drawing.Color.FromArgb(CType(CType(25, Byte), Integer), CType(CType(25, Byte), Integer), CType(CType(25, Byte), Integer))
        Me.SimplaTheme1.Controls.Add(Me.ValidateNET)
        Me.SimplaTheme1.Controls.Add(Me.listAnu)
        Me.SimplaTheme1.Controls.Add(Me.Label4)
        Me.SimplaTheme1.Controls.Add(Me.btn_Update)
        Me.SimplaTheme1.Controls.Add(Me.Xmninim)
        Me.SimplaTheme1.Controls.Add(Me.Cls)
        Me.SimplaTheme1.Controls.Add(Me.xL)
        Me.SimplaTheme1.Controls.Add(Me.LinkLabel1)
        Me.SimplaTheme1.Controls.Add(Me.Label3)
        Me.SimplaTheme1.Controls.Add(Me.AUTOCONNECT)
        Me.SimplaTheme1.Controls.Add(Me.SimplaProgressBar1)
        Me.SimplaTheme1.Controls.Add(Me.GroupBox2)
        Me.SimplaTheme1.Controls.Add(Me.GroupBox1)
        Me.SimplaTheme1.Controls.Add(Me.SimplaButton1)
        Me.SimplaTheme1.Controls.Add(Me.SimplaButton2)
        Me.SimplaTheme1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SimplaTheme1.Location = New System.Drawing.Point(0, 0)
        Me.SimplaTheme1.Name = "SimplaTheme1"
        Me.SimplaTheme1.Size = New System.Drawing.Size(431, 543)
        Me.SimplaTheme1.TabIndex = 0
        '
        'ValidateNET
        '
        Me.ValidateNET.AutoSize = True
        Me.ValidateNET.Location = New System.Drawing.Point(276, 386)
        Me.ValidateNET.Name = "ValidateNET"
        Me.ValidateNET.Size = New System.Drawing.Size(0, 13)
        Me.ValidateNET.TabIndex = 21
        Me.ValidateNET.Visible = False
        '
        'listAnu
        '
        Me.listAnu.FormattingEnabled = True
        Me.listAnu.Items.AddRange(New Object() {"http://mbasic.facebook.com", "http://192.168.1.1", "http://1.1.1.1", "http://6.6.6.6", "http://192.168.1.6", "http://192.168.1.254", " "})
        Me.listAnu.Location = New System.Drawing.Point(450, 43)
        Me.listAnu.Name = "listAnu"
        Me.listAnu.Size = New System.Drawing.Size(208, 82)
        Me.listAnu.TabIndex = 17
        Me.listAnu.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Modern No. 20", 8.249999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.Label4.Location = New System.Drawing.Point(31, 523)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 14)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Check For Update"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_Update
        '
        Me.btn_Update.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.btn_Update.BackgroundImage = CType(resources.GetObject("btn_Update.BackgroundImage"), System.Drawing.Image)
        Me.btn_Update.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_Update.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.btn_Update.FlatAppearance.BorderSize = 0
        Me.btn_Update.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray
        Me.btn_Update.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Info
        Me.btn_Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_Update.ForeColor = System.Drawing.Color.Transparent
        Me.btn_Update.Location = New System.Drawing.Point(9, 523)
        Me.btn_Update.Name = "btn_Update"
        Me.btn_Update.Size = New System.Drawing.Size(16, 16)
        Me.btn_Update.TabIndex = 17
        Me.btn_Update.Text = "Update"
        Me.ToolTip1.SetToolTip(Me.btn_Update, "Check Pembaruan Terbaru")
        Me.btn_Update.UseVisualStyleBackColor = False
        '
        'Xmninim
        '
        Me.Xmninim.BackColor = System.Drawing.Color.Transparent
        Me.Xmninim.BaseColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Xmninim.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Xmninim.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Xmninim.Location = New System.Drawing.Point(381, 9)
        Me.Xmninim.Name = "Xmninim"
        Me.Xmninim.Rounded = False
        Me.Xmninim.Size = New System.Drawing.Size(19, 19)
        Me.Xmninim.TabIndex = 16
        Me.Xmninim.Text = "-"
        Me.Xmninim.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'Cls
        '
        Me.Cls.BackColor = System.Drawing.Color.Transparent
        Me.Cls.BaseColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Cls.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Cls.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cls.Location = New System.Drawing.Point(400, 9)
        Me.Cls.Name = "Cls"
        Me.Cls.Rounded = False
        Me.Cls.Size = New System.Drawing.Size(19, 19)
        Me.Cls.TabIndex = 15
        Me.Cls.Text = "X"
        Me.Cls.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'xL
        '
        Me.xL.AutoSize = True
        Me.xL.BackColor = System.Drawing.Color.Transparent
        Me.xL.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xL.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.xL.Location = New System.Drawing.Point(261, 501)
        Me.xL.Name = "xL"
        Me.xL.Size = New System.Drawing.Size(133, 13)
        Me.xL.TabIndex = 14
        Me.xL.Text = "Status : Premium Version"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LinkLabel1.LinkColor = System.Drawing.Color.Violet
        Me.LinkLabel1.Location = New System.Drawing.Point(33, 504)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(67, 13)
        Me.LinkLabel1.TabIndex = 10
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Report Bug?"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label3.Font = New System.Drawing.Font("Eras Medium ITC", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Yellow
        Me.Label3.Location = New System.Drawing.Point(56, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(319, 19)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "[+] @WIFI.ID FVKIN666 EVOLUTION V.3 [+]"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AUTOCONNECT
        '
        Me.AUTOCONNECT.BackColor = System.Drawing.Color.Transparent
        Me.AUTOCONNECT.Checked = False
        Me.AUTOCONNECT.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaCheckBox.ColorSchemes.Red
        Me.AUTOCONNECT.ForeColor = System.Drawing.Color.Black
        Me.AUTOCONNECT.Location = New System.Drawing.Point(36, 383)
        Me.AUTOCONNECT.Name = "AUTOCONNECT"
        Me.AUTOCONNECT.Size = New System.Drawing.Size(175, 16)
        Me.AUTOCONNECT.TabIndex = 5
        Me.AUTOCONNECT.Text = "Aktifkan Autoconnect"
        '
        'SimplaProgressBar1
        '
        Me.SimplaProgressBar1.BackColor = System.Drawing.Color.Transparent
        Me.SimplaProgressBar1.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaProgressBar.ColorSchemes.Blue
        Me.SimplaProgressBar1.Location = New System.Drawing.Point(36, 414)
        Me.SimplaProgressBar1.Maximum = 100
        Me.SimplaProgressBar1.Name = "SimplaProgressBar1"
        Me.SimplaProgressBar1.ShowPercentage = False
        Me.SimplaProgressBar1.Size = New System.Drawing.Size(358, 27)
        Me.SimplaProgressBar1.TabIndex = 3
        Me.SimplaProgressBar1.Text = "SimplaProgressBar1"
        Me.SimplaProgressBar1.Value = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RB_SERVER15)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER10)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER5)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER14)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER9)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER4)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER13)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER8)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER3)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER12)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER7)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER2)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER11)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER6)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER1)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox2.Font = New System.Drawing.Font("Humanst521 BT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox2.Location = New System.Drawing.Point(36, 198)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(358, 179)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'RB_SERVER15
        '
        Me.RB_SERVER15.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER15.Checked = False
        Me.RB_SERVER15.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER15.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER15.Location = New System.Drawing.Point(248, 148)
        Me.RB_SERVER15.Name = "RB_SERVER15"
        Me.RB_SERVER15.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER15.TabIndex = 17
        Me.RB_SERVER15.Text = "Server 15"
        '
        'RB_SERVER10
        '
        Me.RB_SERVER10.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER10.Checked = False
        Me.RB_SERVER10.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER10.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER10.Location = New System.Drawing.Point(139, 148)
        Me.RB_SERVER10.Name = "RB_SERVER10"
        Me.RB_SERVER10.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER10.TabIndex = 16
        Me.RB_SERVER10.Text = "Server 10"
        '
        'RB_SERVER5
        '
        Me.RB_SERVER5.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER5.Checked = False
        Me.RB_SERVER5.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER5.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER5.Location = New System.Drawing.Point(26, 148)
        Me.RB_SERVER5.Name = "RB_SERVER5"
        Me.RB_SERVER5.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER5.TabIndex = 15
        Me.RB_SERVER5.Text = "Server 5"
        '
        'RB_SERVER14
        '
        Me.RB_SERVER14.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER14.Checked = False
        Me.RB_SERVER14.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER14.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER14.Location = New System.Drawing.Point(248, 117)
        Me.RB_SERVER14.Name = "RB_SERVER14"
        Me.RB_SERVER14.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER14.TabIndex = 14
        Me.RB_SERVER14.Text = "Server 14"
        '
        'RB_SERVER9
        '
        Me.RB_SERVER9.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER9.Checked = False
        Me.RB_SERVER9.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER9.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER9.Location = New System.Drawing.Point(139, 117)
        Me.RB_SERVER9.Name = "RB_SERVER9"
        Me.RB_SERVER9.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER9.TabIndex = 13
        Me.RB_SERVER9.Text = "Server 9"
        '
        'RB_SERVER4
        '
        Me.RB_SERVER4.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER4.Checked = False
        Me.RB_SERVER4.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER4.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER4.Location = New System.Drawing.Point(26, 117)
        Me.RB_SERVER4.Name = "RB_SERVER4"
        Me.RB_SERVER4.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER4.TabIndex = 12
        Me.RB_SERVER4.Text = "Server 4"
        '
        'RB_SERVER13
        '
        Me.RB_SERVER13.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER13.Checked = False
        Me.RB_SERVER13.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER13.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER13.Location = New System.Drawing.Point(248, 87)
        Me.RB_SERVER13.Name = "RB_SERVER13"
        Me.RB_SERVER13.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER13.TabIndex = 11
        Me.RB_SERVER13.Text = "Server 13"
        '
        'RB_SERVER8
        '
        Me.RB_SERVER8.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER8.Checked = False
        Me.RB_SERVER8.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER8.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER8.Location = New System.Drawing.Point(139, 87)
        Me.RB_SERVER8.Name = "RB_SERVER8"
        Me.RB_SERVER8.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER8.TabIndex = 10
        Me.RB_SERVER8.Text = "Server 8"
        '
        'RB_SERVER3
        '
        Me.RB_SERVER3.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER3.Checked = False
        Me.RB_SERVER3.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER3.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER3.Location = New System.Drawing.Point(26, 87)
        Me.RB_SERVER3.Name = "RB_SERVER3"
        Me.RB_SERVER3.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER3.TabIndex = 9
        Me.RB_SERVER3.Text = "Server 3"
        '
        'RB_SERVER12
        '
        Me.RB_SERVER12.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER12.Checked = False
        Me.RB_SERVER12.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER12.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER12.Location = New System.Drawing.Point(248, 56)
        Me.RB_SERVER12.Name = "RB_SERVER12"
        Me.RB_SERVER12.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER12.TabIndex = 8
        Me.RB_SERVER12.Text = "Server 12"
        '
        'RB_SERVER7
        '
        Me.RB_SERVER7.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER7.Checked = False
        Me.RB_SERVER7.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER7.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER7.Location = New System.Drawing.Point(139, 56)
        Me.RB_SERVER7.Name = "RB_SERVER7"
        Me.RB_SERVER7.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER7.TabIndex = 7
        Me.RB_SERVER7.Text = "Server 7"
        '
        'RB_SERVER2
        '
        Me.RB_SERVER2.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER2.Checked = False
        Me.RB_SERVER2.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER2.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER2.Location = New System.Drawing.Point(27, 56)
        Me.RB_SERVER2.Name = "RB_SERVER2"
        Me.RB_SERVER2.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER2.TabIndex = 6
        Me.RB_SERVER2.Text = "Server 2"
        '
        'RB_SERVER11
        '
        Me.RB_SERVER11.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER11.Checked = False
        Me.RB_SERVER11.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER11.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER11.Location = New System.Drawing.Point(248, 25)
        Me.RB_SERVER11.Name = "RB_SERVER11"
        Me.RB_SERVER11.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER11.TabIndex = 5
        Me.RB_SERVER11.Text = "Server 11"
        '
        'RB_SERVER6
        '
        Me.RB_SERVER6.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER6.Checked = False
        Me.RB_SERVER6.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER6.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER6.Location = New System.Drawing.Point(139, 25)
        Me.RB_SERVER6.Name = "RB_SERVER6"
        Me.RB_SERVER6.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER6.TabIndex = 4
        Me.RB_SERVER6.Text = "Server 6"
        '
        'RB_SERVER1
        '
        Me.RB_SERVER1.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER1.Checked = False
        Me.RB_SERVER1.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER1.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER1.Location = New System.Drawing.Point(27, 25)
        Me.RB_SERVER1.Name = "RB_SERVER1"
        Me.RB_SERVER1.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER1.TabIndex = 3
        Me.RB_SERVER1.Text = "Server 1"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.LightGreen
        Me.Label1.Location = New System.Drawing.Point(154, 1)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "SERVER"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.StatusConn)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Font = New System.Drawing.Font("Humanst521 BT", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox1.Location = New System.Drawing.Point(7, 43)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(416, 153)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'StatusConn
        '
        Me.StatusConn.AutoSize = True
        Me.StatusConn.Location = New System.Drawing.Point(423, 115)
        Me.StatusConn.Name = "StatusConn"
        Me.StatusConn.Size = New System.Drawing.Size(66, 14)
        Me.StatusConn.TabIndex = 5
        Me.StatusConn.Text = "status Conn"
        Me.StatusConn.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Humanst521 BT", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Turquoise
        Me.Label2.Location = New System.Drawing.Point(148, -3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(121, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "NETWORK STATUS"
        '
        'ListBox1
        '
        Me.ListBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(25, Byte), Integer), CType(CType(25, Byte), Integer), CType(CType(25, Byte), Integer))
        Me.ListBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ListBox1.ForeColor = System.Drawing.Color.LimeGreen
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 14
        Me.ListBox1.Location = New System.Drawing.Point(2, 10)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(411, 140)
        Me.ListBox1.TabIndex = 4
        '
        'SimplaButton1
        '
        Me.SimplaButton1.BackColor = System.Drawing.Color.Transparent
        Me.SimplaButton1.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton.ColorSchemes.DarkGray
        Me.SimplaButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SimplaButton1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.SimplaButton1.Location = New System.Drawing.Point(36, 449)
        Me.SimplaButton1.Name = "SimplaButton1"
        Me.SimplaButton1.Size = New System.Drawing.Size(358, 49)
        Me.SimplaButton1.TabIndex = 2
        Me.SimplaButton1.Text = "HAJAR GAN"
        '
        'SimplaButton2
        '
        Me.SimplaButton2.BackColor = System.Drawing.Color.Transparent
        Me.SimplaButton2.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton.ColorSchemes.DarkGray
        Me.SimplaButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SimplaButton2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.SimplaButton2.Location = New System.Drawing.Point(36, 449)
        Me.SimplaButton2.Name = "SimplaButton2"
        Me.SimplaButton2.Size = New System.Drawing.Size(358, 49)
        Me.SimplaButton2.TabIndex = 6
        Me.SimplaButton2.Text = "RESTART"
        '
        'ANDA_MALING
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(431, 543)
        Me.Controls.Add(Me.SimplaTheme1)
        Me.Controls.Add(Me.jampe_jampe)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ANDA_MALING"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.SimplaTheme1.ResumeLayout(False)
        Me.SimplaTheme1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SimplaButton2 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RB_SERVER12 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER7 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER2 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER11 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER6 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER1 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SimplaButton1 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton
    Friend WithEvents SimplaProgressBar1 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaProgressBar
    Friend WithEvents AUTOCONNECT As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaCheckBox
    Friend WithEvents SimplaTheme1 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaTheme
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents jampe_jampe As System.Windows.Forms.WebBrowser
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents RB_SERVER15 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER10 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER5 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER14 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER9 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER4 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER13 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER8 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER3 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents xL As System.Windows.Forms.Label
    Friend WithEvents Cls As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton
    Friend WithEvents Xmninim As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton
    Friend WithEvents listAnu As System.Windows.Forms.ListBox
    Friend WithEvents btn_Update As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ValidateNET As System.Windows.Forms.Label
    Friend WithEvents CheckCon As System.Windows.Forms.Timer
    Friend WithEvents StatusConn As System.Windows.Forms.Label
    Friend WithEvents DirectKeAnu As System.Windows.Forms.Timer

End Class
