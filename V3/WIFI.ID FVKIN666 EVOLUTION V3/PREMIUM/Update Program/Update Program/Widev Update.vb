﻿Imports System.Net
Public Class Widev_Update
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        ProgressBar1.Value += 1
        Button1.Enabled = False
        If ProgressBar1.Value = 5 Then
            ListBox1.Items.Add("Checking..")
        ElseIf ProgressBar1.Value = 60 Then
            CHECKkoneksi()
        ElseIf ProgressBar1.Value = 70 Then
            ListBox1.Items.Add("Mengubungkan..")
        ElseIf ProgressBar1.Value = 80 Then
            ListBox1.Items.Add("Check Pembaruan..")
        ElseIf ProgressBar1.Value = 88 Then
            ListBox1.Items.Add("Final Check..")
        ElseIf ProgressBar1.Value = 100 Then
            Timer1.Stop()
            UpdateApp()
            Button1.Enabled = True
            Button1.Text = "Check For Update?"

            ProgressBar1.Cursor = Cursors.Default ' Cursor Akan Kembali Semula
        End If
    End Sub
    Private Sub CHECKkoneksi()
        If My.Computer.Network.IsAvailable = False Then ' Jika koneksi tidak tersedia..
            Timer1.Stop()
            '-----------------------------------------------------------------------
            ListBox1.Items.Add("Tidak Terkoneksi Ke Internet")
            MsgBox("No Connection Available..", MsgBoxStyle.Critical, "")
            ProgressBar1.Value = 0 ' Cursor Kembali Ke Nol
            ProgressBar1.Cursor = Cursors.Default ' Cursor Akan Kembali Semula
            '-----------------------------------------------------------------------'

            Button1.Text = "Check For Update?" '//Button kembali tertulis Check For Update
            Button1.Enabled = True
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ProgressBar1.Cursor = Cursors.WaitCursor ' jika di klik, Akan menunggu

        ListBox1.Items.Clear()
        Button1.Text = "Cheking Update..."
        ProgressBar1.Value = 0
        Timer1.Start()
    End Sub
    Private Sub UpdateApp()
        Try
            ' ----------------------------------------------------- Deklarasi -----------------------------------------------------
            Dim req As HttpWebRequest = HttpWebRequest.
                                     Create("https://dl.dropbox.com/s/fgew95dl48upxll/version.txt?dl=0") ' Hubungi

            Dim respon As HttpWebResponse = req.GetResponse ' Menampilkan data / Mengambil respon dari req

            Dim bacaRespon As New IO.StreamReader(respon.GetResponseStream) ' membaca data dan copy respon web
            Dim versi_N As String = bacaRespon.ReadToEnd()

            Dim versi_Lst As String = Application.ProductVersion '// Versi sekarang
            '----------------------------------------------------------------------------------------------------------------------



            If Not versi_N.Contains(versi_Lst) Then ' Jika versi baru adalah / Sama dengan versi Lama
                Dim Ys = MsgBox("Versi Baru Tersedia," _
                                        & vbNewLine & "Update program ke yang baru?", MsgBoxStyle.YesNo, "")

                If Ys = MsgBoxResult.Yes Then
                    WebBrowser1.Navigate("https://dl.dropbox.com/s/b9tlrziv20adwy9/WIDEV%203.3%20%28%20PREMIUM%20VERSION%20%29.zip?dl=0")
                    ListBox1.Items.Add("Pembaruan Baru Tersedia")
                End If
            Else
                ListBox1.Items.Add("Tidak Ada Pembaruan Terbaru")
                MsgBox("Anda Telah berada pada Versi Baru", MsgBoxStyle.Information, "")
            End If

        Catch ex As Exception
            ListBox1.Items.Add("404")
            MsgBox("Terjadi Kesalahan, Check Koneksi Anda" & vbNewLine & _
                   "atau hubungi Admin", MsgBoxStyle.Exclamation, "")
        End Try
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        WebBrowser1.Visible = False
    End Sub
End Class