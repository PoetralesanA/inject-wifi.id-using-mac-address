﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Widev Update")> 
<Assembly: AssemblyDescription("Dont Touch My App!")> 
<Assembly: AssemblyCompany("C.S.D Team")> 
<Assembly: AssemblyProduct("WIFI.ID INJECTION")> 
<Assembly: AssemblyCopyright("Copyright ©  2018")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("1a0744b2-28ed-4431-9b54-40e1ecea46e2")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.0.0.8")> 
<Assembly: AssemblyFileVersion("3.0.0.8")> 
