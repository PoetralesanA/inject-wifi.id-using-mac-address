﻿Imports System.Net
Imports System.Threading
'Version 3.6 16 juni 2018
' Remove ObsoleteAttribute
' Improve Wireless Adapter

' Msgbox Connection sukses ( 19 Juni 2018 )
Public Class ANDA_MALING
    '//   Move Form /////////
    Dim mouseX, mouseY As Integer
    Dim p As New Point
    '-----------------------'

    Dim PESAN As New mb.ShowMessagebox
    Dim KIRIM As New Net.NetworkInformation.Ping
    Dim result As Net.NetworkInformation.PingReply

    Public Sub ValInterface() '//Validasi Interface
        Dim NetInterface() As System.Net.NetworkInformation.NetworkInterface =
            System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces
        Dim Checked As Boolean
        For Each ifconfig In NetInterface
            If ifconfig.NetworkInterfaceType = System.Net.NetworkInformation.
                NetworkInterfaceType.Wireless80211 Then

                ValidateNET.Text = ifconfig.Name
                Checked = True
            End If
        Next
        If ValidateNET.Text = "" Then
            MsgBox("Error Code 404" & vbNewLine &
                 "Contact Admin!", MsgBoxStyle.Critical, "404")
            Process.Start("https://www.facebook.com/poetralesana")
            Me.Close()
        Else
            If Not Checked Then
                MsgBox("Error Code 0x80070035" & vbNewLine &
                 "Contact Admin!", MsgBoxStyle.Critical, "0x80070035")
                Process.Start("https://www.facebook.com/poetralesana")
                Me.Close()
            End If
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        BackgroundWorker1.RunWorkerAsync()
        SimplaButton1.Enabled = False
        SimplaButton2.Enabled = False

        ValInterface()

        CheckCon.Enabled = True 'msgbox conn

    End Sub
    Public Sub Hajar(sender As Object, e As EventArgs) Handles SimplaButton1.Click
        DirectKeAnu.Enabled = True ' Direct

        AUTOCONNECT.Enabled = False
        ' Pemilihan Server
        If RB_SERVER1.Checked = True Then
            SERVER.SERVER1()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_1() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER2.Checked = True Then
            SERVER.SERVER2()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_2() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER3.Checked = True Then
            SERVER.SERVER3()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_3() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER4.Checked = True Then
            SERVER.SERVER4()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_4() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER5.Checked = True Then
            SERVER.SERVER5()
            SimplaProgressBar1.ShowPercentage = True
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_5() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER6.Checked = True Then
            SERVER.SERVER6()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_6() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER7.Checked = True Then
            SERVER.SERVER7()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_7() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER8.Checked = True Then
            SERVER.SERVER8()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_8() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER9.Checked = True Then
            SERVER.SERVER9()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_9() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER10.Checked = True Then
            SERVER.SERVER10()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_10() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER11.Checked = True Then
            SERVER.SERVER11()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_11() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER12.Checked = True Then
            SERVER.SERVER12()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_12() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER13.Checked = True Then
            SERVER.SERVER13()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_13() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER14.Checked = True Then
            SERVER.SERVER14()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_14() '// FALSE CHECK BUTTON
        ElseIf RB_SERVER15.Checked = True Then
            SERVER.SERVER15()
            SimplaButton1.Visible = False
            SimplaButton2.Visible = True
            SimplaProgressBar1.ShowPercentage = True
            Timer1.Start()
            FALSECHECK.FALSE_15() '// FALSE CHECK BUTTON
        Else
            MsgBox("Pilih Server Dulu Boskuu.. :*", MsgBoxStyle.Exclamation, "OPSSS!!")
            AUTOCONNECT.Enabled = True
        End If
    End Sub
    Private Sub Restart(sender As Object, e As EventArgs) Handles SimplaButton2.Click
        SimplaButton2.Visible = False
        SimplaButton1.Visible = True

        AUTOCONNECT.Enabled = True
        SimplaProgressBar1.Value = 0
        SimplaProgressBar1.ShowPercentage = False
        CLOSE_SERVER.CLEAR()
        RestrBtnPrgrs.Show()
        Me.Close()
    End Sub
    Private Sub AUTOCONNECT_CheckedChanged(sender As Object) Handles AUTOCONNECT.CheckedChanged
        If AUTOCONNECT.Checked = True Then
            SimplaButton1.Enabled = True
        Else
            SimplaButton1.Enabled = False
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If SimplaProgressBar1.Value < 100 Then
            SimplaProgressBar1.Value += 1

            If SimplaProgressBar1.Value = 1 Then
                WIFI_EN_DISBLE.DIE()
            ElseIf SimplaProgressBar1.Value = 50 Then
                WIFI_EN_DISBLE.LIFE()
            ElseIf SimplaProgressBar1.Value = 100 Then
                Timer1.Stop()

                CLOSE_SERVER.CLEAR()
                PESANSUKSES()
                SimplaButton2.Enabled = True
            End If
        End If
    End Sub
    ' PESAN SUCESS
    Private Sub PESANSUKSES()
        PESAN.Fonts(New Font("Arial", 9, FontStyle.Regular), mb.TextColor.Green)
        PESAN.ShowBox("INJECT SUCCESS", mb.MStyle.ok, mb.FStyle.Information, ":*")
        CLOSE_SERVER.CLEAR() ' del Servr
    End Sub
    Private Sub VisitedChannel(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Process.Start("http://www.facebook.com/poetralesana")
    End Sub
    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try
            result = KIRIM.Send("172.217.194.91")
        Catch ex As Exception
            System.Threading.Thread.Sleep(3000)
        End Try
    End Sub
    ' // ListAnu
    ' List Box Select Link Direct
    Private Sub selectAnu()
        Try
            listAnu.SelectedIndex += 1
            If listAnu.SelectedItem = " " Then
                listAnu.SelectedIndex = 0
            End If
            jampe_jampe.Navigate(listAnu.SelectedItem)
        Catch miss As Exception

        End Try
    End Sub
    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted

        Try
            If result.Status = NetworkInformation.IPStatus.TimedOut Then
                ListBox1.Items.Add("Request Time Out")
                ListBox1.SelectedIndex += 1

                selectAnu() ' List Akun Connect
            ElseIf result.Status = NetworkInformation.IPStatus.DestinationHostUnreachable Then
                ListBox1.Items.Add("Destination Host Unreachable")
                ListBox1.SelectedIndex += 1
            Else
                PINGHANDLE()
            End If
            '/////////// Fungsi dibawah ini untuk menHapus item jika lebih dari 9 //////////

            'If ListBox1.SelectedIndex > 9 Then
            '    ListBox1.Items.Clear()
            'End If
            BackgroundWorker1.RunWorkerAsync()
        Catch ex As Exception
            MsgBox("Network Busy!!", MsgBoxStyle.Critical, "Try Again")
            Me.Close()
        End Try
    End Sub
    Private Sub PINGHANDLE()
        Try
            If ListBox1.Items.Add("Reply From:" & " 172.217.194.91" & " Time=" & result.RoundtripTime.ToString &
                                  "ms" & " Time To Live=" & result.Options.Ttl.ToString) Then
                ListBox1.SelectedIndex += 1
                StatusConn.Text = "ok"
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub KELUAR(sender As Object, e As EventArgs) Handles Cls.Click
        ' //// Keluar Form
        If SimplaButton1.Visible = True Then
            Me.Close()
        ElseIf SimplaButton1.Visible = False Then
            Dim klwr = MsgBox("Proses sedang berjalan.." & vbNewLine & _
                            "Keluar dapat menyebabkan kurang Efisiennya kerja Software" & vbNewLine & "" & vbNewLine & _
                            "Tetap keluar?", MsgBoxStyle.YesNo, "Alert!!!")
            If klwr = MsgBoxResult.Yes Then
                CLOSE_SERVER.CLEAR()
                Me.Close()
            End If
        End If
    End Sub

    Private Sub mnim(sender As Object, e As EventArgs) Handles Xmninim.Click
        WindowState = FormWindowState.Minimized
    End Sub

    Private Sub MsDown(sender As Object, e As MouseEventArgs) Handles Label3.MouseDown
        '// Move --------
        mouseX = Control.MousePosition.X - Me.Location.X
        mouseY = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub MsMove(sender As Object, e As MouseEventArgs) Handles Label3.MouseMove
        If e.Button = Windows.Forms.MouseButtons.Left Then

            p = Control.MousePosition
            p.X -= mouseX
            p.Y -= mouseY

            Me.Location = p
            Application.DoEvents()
        End If
    End Sub

    Private Sub ButtonUpdate(sender As Object, e As EventArgs) Handles btn_Update.Click
        Widev_Update.Show()
    End Sub

    Private Sub CheckCon_Tick(sender As Object, e As EventArgs) Handles CheckCon.Tick
        If StatusConn.Text = "ok" Then
            CheckCon.Enabled = False
            MsgBox("Happy Browsing.. :)", MsgBoxStyle.Information, "")
        End If
    End Sub
    Private Sub DirectKaka(sender As Object, e As EventArgs) Handles DirectKeAnu.Tick
        If jampe_jampe.DocumentText.Contains("redirected") Then
            DirectKeAnu.Enabled = False
            Process.Start(listAnu.SelectedItem)
        End If
    End Sub
End Class

'// CATATAN Perubahan
' 1. Improve Code

'// Update
' 1. Fix False Detection Network ( Rencana )
' 2. Remove Network Registry ( Rencana )