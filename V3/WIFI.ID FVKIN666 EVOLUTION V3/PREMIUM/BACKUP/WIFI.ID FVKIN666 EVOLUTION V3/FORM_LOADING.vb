﻿Imports System.Net.NetworkInformation
Public Class FORM_LOADING
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim i As Double
        Static cont As Integer

        For i = 1 To 100
            Label1.Text = ProgressBar1.Value & " %"
        Next
        'Progrsbrr
        If cont <= 100 Then
            ProgressBar1.Value = cont
            cont = cont + 1
        ElseIf My.Computer.Network.IsAvailable = False Then
            Timer1.Stop()
            MessageBox.Show("Sorry, Network is unavailable!")
            Me.Close()
        Else
            Timer1.Stop()
            ''------------------------------- SSID CHeck -------------------------------''
            Try
                Dim processinfo As New ProcessStartInfo("netsh", "wlan show interfaces")
                processinfo.UseShellExecute = False
                processinfo.RedirectStandardOutput = True


                Dim process As New Process()
                process.StartInfo = processinfo
                process.Start().ToString()

                Dim output As String
                Using Rd_CMD As System.IO.StreamReader = process.StandardOutput
                    output = Rd_CMD.ReadToEnd

                End Using

                lb_SSid.Text = (output) 'tampil output CMD

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "")
            End Try

            If Label_MY_IP.Text = "10" Then '// Jika ip bukan awalan 10, maka Akan Close
                ANDA_MALING.Show()
                Me.Close()
            Else
                If lb_SSid.Text.Contains("@wifi.id") Then 'Baca Output SSID
                    ANDA_MALING.Show()
                    Me.Close()
                Else
                    MessageBox.Show("Hanya bisa digunakan pada Area @wifi.id" _
                                      , "Undetect")
                    Me.Close()
                End If
            End If
        End If '// Timer

        '// Coding lama Deteksi IP
        '' // Validasi IP
        'If Label_MY_IP.Text = "10" Then '// Jika ip bukan awalan 10, maka Akan Close
        '    ANDA_MALING.Show()
        '    Me.Close()
        'Else
        '    MessageBox.Show("Hanya bisa digunakan pada Area @wifi.id" _
        '                    , "Undetect") '// Title
        '    Me.Close()
        'End If

        ' Di ganti pada 5 juni 2018
    End Sub
    <Obsolete>
    Private Sub IPCHECK() '// Check IP DNS WIFI.ID
        Dim IPHOST As String

        System.Net.Dns.GetHostName() '// Mengambil nama host

        IPHOST = System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString() '// false = tidak dimunculkan

        Label_MY_IP.Text = Microsoft.VisualBasic.Left(IPHOST, 2) '// Label mengambil IP
    End Sub
    <Obsolete>
    Private Sub FORM_LOADING_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        IPCHECK()
        Timer1.Start()
    End Sub
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        Label2.ForeColor = Color.Black
    End Sub
    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        Label2.ForeColor = Color.Red
    End Sub
End Class