﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RestrBtnPrgrs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RestrBtnPrgrs))
        Me.LoadingCircleX = New MRG.Controls.UI.LoadingCircle()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.HideLoading = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'LoadingCircleX
        '
        Me.LoadingCircleX.Active = False
        Me.LoadingCircleX.Color = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LoadingCircleX.InnerCircleRadius = 8
        Me.LoadingCircleX.Location = New System.Drawing.Point(83, 1)
        Me.LoadingCircleX.Name = "LoadingCircleX"
        Me.LoadingCircleX.NumberSpoke = 24
        Me.LoadingCircleX.OuterCircleRadius = 9
        Me.LoadingCircleX.RotationSpeed = 100
        Me.LoadingCircleX.Size = New System.Drawing.Size(114, 99)
        Me.LoadingCircleX.SpokeThickness = 4
        Me.LoadingCircleX.StylePreset = MRG.Controls.UI.LoadingCircle.StylePresets.IE7
        Me.LoadingCircleX.TabIndex = 0
        Me.LoadingCircleX.Text = "LoadingCircle1"
        '
        'Timer1
        '
        Me.Timer1.Interval = 20
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Chiller", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(88, 94)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 27)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Please Wait"
        '
        'HideLoading
        '
        Me.HideLoading.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.HideLoading.AutoSize = True
        Me.HideLoading.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.HideLoading.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HideLoading.ForeColor = System.Drawing.Color.GreenYellow
        Me.HideLoading.Location = New System.Drawing.Point(129, 121)
        Me.HideLoading.Name = "HideLoading"
        Me.HideLoading.Size = New System.Drawing.Size(23, 25)
        Me.HideLoading.TabIndex = 2
        Me.HideLoading.Text = "1"
        Me.HideLoading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.HideLoading.Visible = False
        '
        'RestrBtnPrgrs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(280, 149)
        Me.Controls.Add(Me.HideLoading)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LoadingCircleX)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "RestrBtnPrgrs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Restart"
        Me.TransparencyKey = System.Drawing.Color.Black
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LoadingCircleX As MRG.Controls.UI.LoadingCircle
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents HideLoading As System.Windows.Forms.Label
End Class
