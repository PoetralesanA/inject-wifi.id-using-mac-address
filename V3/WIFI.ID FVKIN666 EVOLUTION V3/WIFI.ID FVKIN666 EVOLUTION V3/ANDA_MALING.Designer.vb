﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ANDA_MALING
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ANDA_MALING))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.jampe_jampe = New System.Windows.Forms.WebBrowser()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.CheckCon = New System.Windows.Forms.Timer(Me.components)
        Me.SimplaTheme1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaTheme()
        Me.StatusConn = New System.Windows.Forms.Label()
        Me.ValidateNET = New System.Windows.Forms.Label()
        Me.Minim = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton()
        Me.cls = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.AUTOCONNECT = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaCheckBox()
        Me.SimplaProgressBar1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaProgressBar()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RB_SERVER6 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER5 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER4 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER3 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER2 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.RB_SERVER1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.listAnu = New System.Windows.Forms.ListBox()
        Me.SimplaButton1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton()
        Me.SimplaButton2 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton()
        Me.DirectKeAnu = New System.Windows.Forms.Timer(Me.components)
        Me.BgW_Surv = New System.ComponentModel.BackgroundWorker()
        Me.SimplaTheme1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Interval = 125
        '
        'jampe_jampe
        '
        Me.jampe_jampe.Location = New System.Drawing.Point(0, 0)
        Me.jampe_jampe.MinimumSize = New System.Drawing.Size(20, 20)
        Me.jampe_jampe.Name = "jampe_jampe"
        Me.jampe_jampe.ScriptErrorsSuppressed = True
        Me.jampe_jampe.Size = New System.Drawing.Size(420, 236)
        Me.jampe_jampe.TabIndex = 5
        Me.jampe_jampe.Url = New System.Uri("", System.UriKind.Relative)
        Me.jampe_jampe.Visible = False
        '
        'BackgroundWorker1
        '
        '
        'CheckCon
        '
        Me.CheckCon.Interval = 1
        '
        'SimplaTheme1
        '
        Me.SimplaTheme1.AllowDrop = True
        Me.SimplaTheme1.BackColor = System.Drawing.Color.FromArgb(CType(CType(25, Byte), Integer), CType(CType(25, Byte), Integer), CType(CType(25, Byte), Integer))
        Me.SimplaTheme1.Controls.Add(Me.StatusConn)
        Me.SimplaTheme1.Controls.Add(Me.ValidateNET)
        Me.SimplaTheme1.Controls.Add(Me.Minim)
        Me.SimplaTheme1.Controls.Add(Me.cls)
        Me.SimplaTheme1.Controls.Add(Me.LinkLabel1)
        Me.SimplaTheme1.Controls.Add(Me.Label3)
        Me.SimplaTheme1.Controls.Add(Me.AUTOCONNECT)
        Me.SimplaTheme1.Controls.Add(Me.SimplaProgressBar1)
        Me.SimplaTheme1.Controls.Add(Me.GroupBox2)
        Me.SimplaTheme1.Controls.Add(Me.GroupBox1)
        Me.SimplaTheme1.Controls.Add(Me.SimplaButton1)
        Me.SimplaTheme1.Controls.Add(Me.SimplaButton2)
        Me.SimplaTheme1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SimplaTheme1.Location = New System.Drawing.Point(0, 0)
        Me.SimplaTheme1.Name = "SimplaTheme1"
        Me.SimplaTheme1.Size = New System.Drawing.Size(431, 434)
        Me.SimplaTheme1.TabIndex = 0
        '
        'StatusConn
        '
        Me.StatusConn.AutoSize = True
        Me.StatusConn.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.StatusConn.Location = New System.Drawing.Point(429, 412)
        Me.StatusConn.Name = "StatusConn"
        Me.StatusConn.Size = New System.Drawing.Size(63, 13)
        Me.StatusConn.TabIndex = 14
        Me.StatusConn.Text = "status Conn"
        Me.StatusConn.Visible = False
        '
        'ValidateNET
        '
        Me.ValidateNET.AutoSize = True
        Me.ValidateNET.Location = New System.Drawing.Point(439, 292)
        Me.ValidateNET.Name = "ValidateNET"
        Me.ValidateNET.Size = New System.Drawing.Size(0, 13)
        Me.ValidateNET.TabIndex = 13
        Me.ValidateNET.Visible = False
        '
        'Minim
        '
        Me.Minim.BackColor = System.Drawing.Color.Transparent
        Me.Minim.BaseColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.Minim.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Minim.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Minim.Location = New System.Drawing.Point(378, 9)
        Me.Minim.Name = "Minim"
        Me.Minim.Rounded = False
        Me.Minim.Size = New System.Drawing.Size(20, 19)
        Me.Minim.TabIndex = 12
        Me.Minim.Text = "-"
        Me.Minim.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'cls
        '
        Me.cls.BackColor = System.Drawing.Color.Transparent
        Me.cls.BaseColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.cls.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cls.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cls.Location = New System.Drawing.Point(397, 9)
        Me.cls.Name = "cls"
        Me.cls.Rounded = False
        Me.cls.Size = New System.Drawing.Size(20, 19)
        Me.cls.TabIndex = 11
        Me.cls.Text = "X"
        Me.cls.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LinkLabel1.LinkColor = System.Drawing.Color.Violet
        Me.LinkLabel1.Location = New System.Drawing.Point(33, 407)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(67, 13)
        Me.LinkLabel1.TabIndex = 10
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Report Bug?"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label3.Font = New System.Drawing.Font("Eras Medium ITC", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Yellow
        Me.Label3.Location = New System.Drawing.Point(56, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(319, 19)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "[+] @WIFI.ID FVKIN666 EVOLUTION V.3 [+]"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AUTOCONNECT
        '
        Me.AUTOCONNECT.BackColor = System.Drawing.Color.Transparent
        Me.AUTOCONNECT.Checked = False
        Me.AUTOCONNECT.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaCheckBox.ColorSchemes.Red
        Me.AUTOCONNECT.ForeColor = System.Drawing.Color.Black
        Me.AUTOCONNECT.Location = New System.Drawing.Point(36, 289)
        Me.AUTOCONNECT.Name = "AUTOCONNECT"
        Me.AUTOCONNECT.Size = New System.Drawing.Size(175, 16)
        Me.AUTOCONNECT.TabIndex = 5
        Me.AUTOCONNECT.Text = "Aktifkan Autoconnect"
        '
        'SimplaProgressBar1
        '
        Me.SimplaProgressBar1.BackColor = System.Drawing.Color.Transparent
        Me.SimplaProgressBar1.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaProgressBar.ColorSchemes.Blue
        Me.SimplaProgressBar1.Location = New System.Drawing.Point(36, 320)
        Me.SimplaProgressBar1.Maximum = 100
        Me.SimplaProgressBar1.Name = "SimplaProgressBar1"
        Me.SimplaProgressBar1.ShowPercentage = False
        Me.SimplaProgressBar1.Size = New System.Drawing.Size(358, 27)
        Me.SimplaProgressBar1.TabIndex = 3
        Me.SimplaProgressBar1.Text = "SimplaProgressBar1"
        Me.SimplaProgressBar1.Value = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RB_SERVER6)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER5)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER4)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER3)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER2)
        Me.GroupBox2.Controls.Add(Me.RB_SERVER1)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox2.Font = New System.Drawing.Font("Humanst521 BT", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox2.Location = New System.Drawing.Point(36, 198)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(358, 85)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'RB_SERVER6
        '
        Me.RB_SERVER6.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER6.Checked = False
        Me.RB_SERVER6.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER6.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER6.Location = New System.Drawing.Point(246, 53)
        Me.RB_SERVER6.Name = "RB_SERVER6"
        Me.RB_SERVER6.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER6.TabIndex = 8
        Me.RB_SERVER6.Text = "Server 6"
        '
        'RB_SERVER5
        '
        Me.RB_SERVER5.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER5.Checked = False
        Me.RB_SERVER5.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER5.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER5.Location = New System.Drawing.Point(137, 53)
        Me.RB_SERVER5.Name = "RB_SERVER5"
        Me.RB_SERVER5.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER5.TabIndex = 7
        Me.RB_SERVER5.Text = "Server 5"
        '
        'RB_SERVER4
        '
        Me.RB_SERVER4.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER4.Checked = False
        Me.RB_SERVER4.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER4.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER4.Location = New System.Drawing.Point(25, 53)
        Me.RB_SERVER4.Name = "RB_SERVER4"
        Me.RB_SERVER4.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER4.TabIndex = 6
        Me.RB_SERVER4.Text = "Server 4"
        '
        'RB_SERVER3
        '
        Me.RB_SERVER3.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER3.Checked = False
        Me.RB_SERVER3.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER3.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER3.Location = New System.Drawing.Point(246, 22)
        Me.RB_SERVER3.Name = "RB_SERVER3"
        Me.RB_SERVER3.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER3.TabIndex = 5
        Me.RB_SERVER3.Text = "Server 3"
        '
        'RB_SERVER2
        '
        Me.RB_SERVER2.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER2.Checked = False
        Me.RB_SERVER2.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER2.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER2.Location = New System.Drawing.Point(137, 22)
        Me.RB_SERVER2.Name = "RB_SERVER2"
        Me.RB_SERVER2.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER2.TabIndex = 4
        Me.RB_SERVER2.Text = "Server 2"
        '
        'RB_SERVER1
        '
        Me.RB_SERVER1.BackColor = System.Drawing.Color.Transparent
        Me.RB_SERVER1.Checked = False
        Me.RB_SERVER1.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton.ColorSchemes.Green
        Me.RB_SERVER1.ForeColor = System.Drawing.Color.Black
        Me.RB_SERVER1.Location = New System.Drawing.Point(25, 22)
        Me.RB_SERVER1.Name = "RB_SERVER1"
        Me.RB_SERVER1.Size = New System.Drawing.Size(85, 16)
        Me.RB_SERVER1.TabIndex = 3
        Me.RB_SERVER1.Text = "Server 1"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.LightGreen
        Me.Label1.Location = New System.Drawing.Point(152, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "SERVER"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Controls.Add(Me.listAnu)
        Me.GroupBox1.Font = New System.Drawing.Font("Humanst521 BT", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox1.Location = New System.Drawing.Point(7, 43)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(416, 153)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Humanst521 BT", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Turquoise
        Me.Label2.Location = New System.Drawing.Point(148, -3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(121, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "NETWORK STATUS"
        '
        'ListBox1
        '
        Me.ListBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(25, Byte), Integer), CType(CType(25, Byte), Integer), CType(CType(25, Byte), Integer))
        Me.ListBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ListBox1.ForeColor = System.Drawing.Color.LimeGreen
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 14
        Me.ListBox1.Location = New System.Drawing.Point(2, 10)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(411, 140)
        Me.ListBox1.TabIndex = 4
        '
        'listAnu
        '
        Me.listAnu.FormattingEnabled = True
        Me.listAnu.ItemHeight = 14
        Me.listAnu.Items.AddRange(New Object() {"http://mbasic.facebook.com", "http://192.168.1.1", "http://6.6.6.6", "http://8.8.8.8", "http://192.168.1.254", "http://192.168.1.2", " "})
        Me.listAnu.Location = New System.Drawing.Point(261, 40)
        Me.listAnu.Name = "listAnu"
        Me.listAnu.Size = New System.Drawing.Size(135, 102)
        Me.listAnu.TabIndex = 5
        '
        'SimplaButton1
        '
        Me.SimplaButton1.BackColor = System.Drawing.Color.Transparent
        Me.SimplaButton1.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton.ColorSchemes.DarkGray
        Me.SimplaButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SimplaButton1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.SimplaButton1.Location = New System.Drawing.Point(36, 355)
        Me.SimplaButton1.Name = "SimplaButton1"
        Me.SimplaButton1.Size = New System.Drawing.Size(358, 49)
        Me.SimplaButton1.TabIndex = 2
        Me.SimplaButton1.Text = "HAJAR GAN"
        '
        'SimplaButton2
        '
        Me.SimplaButton2.BackColor = System.Drawing.Color.Transparent
        Me.SimplaButton2.ColorScheme = WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton.ColorSchemes.DarkGray
        Me.SimplaButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SimplaButton2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.SimplaButton2.Location = New System.Drawing.Point(36, 355)
        Me.SimplaButton2.Name = "SimplaButton2"
        Me.SimplaButton2.Size = New System.Drawing.Size(358, 49)
        Me.SimplaButton2.TabIndex = 6
        Me.SimplaButton2.Text = "RESTART"
        '
        'DirectKeAnu
        '
        Me.DirectKeAnu.Interval = 1
        '
        'BgW_Surv
        '
        '
        'ANDA_MALING
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(431, 434)
        Me.Controls.Add(Me.SimplaTheme1)
        Me.Controls.Add(Me.jampe_jampe)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ANDA_MALING"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.SimplaTheme1.ResumeLayout(False)
        Me.SimplaTheme1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SimplaButton2 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RB_SERVER6 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER5 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER4 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER3 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER2 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents RB_SERVER1 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaRadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SimplaButton1 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaButton
    Friend WithEvents SimplaProgressBar1 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaProgressBar
    Friend WithEvents AUTOCONNECT As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaCheckBox
    Friend WithEvents SimplaTheme1 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaTheme
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents jampe_jampe As System.Windows.Forms.WebBrowser
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents cls As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton
    Friend WithEvents Minim As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton
    Friend WithEvents listAnu As System.Windows.Forms.ListBox
    Friend WithEvents ValidateNET As System.Windows.Forms.Label
    Friend WithEvents CheckCon As System.Windows.Forms.Timer
    Friend WithEvents StatusConn As System.Windows.Forms.Label
    Friend WithEvents DirectKeAnu As System.Windows.Forms.Timer
    Friend WithEvents BgW_Surv As System.ComponentModel.BackgroundWorker

End Class
