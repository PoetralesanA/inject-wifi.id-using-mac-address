﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("WIFI.ID GENERATOR")> 
<Assembly: AssemblyDescription("Dont Touch My App!")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("PoetralesanA Security")> 
<Assembly: AssemblyCopyright("Copyright © PoetralesanA 2018")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("b7dab7ae-4630-474c-bc98-971fd67a5e65")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.0.0")> 
<Assembly: AssemblyFileVersion("2.0.0.0")> 

<Assembly: NeutralResourcesLanguageAttribute("en-US")> 