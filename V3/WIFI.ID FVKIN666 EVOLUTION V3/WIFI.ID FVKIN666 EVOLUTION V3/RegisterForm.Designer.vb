﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RegisterForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RegisterForm))
        Me.HWID_REGISTER = New WIFI.ID_FVKIN666_EVOLUTION_V3.FormSkin()
        Me.FlatTextBox1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox()
        Me.FlatGroupBox2 = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatGroupBox()
        Me.KEY = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox()
        Me.GetRegedit = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox()
        Me.SimplaControlBox1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaControlBox()
        Me.FlatButton1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton()
        Me.SECRET_CODE = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox()
        Me.FlatGroupBox1 = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatGroupBox()
        Me.HWID = New WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox()
        Me.HWID_REGISTER.SuspendLayout()
        Me.FlatGroupBox2.SuspendLayout()
        Me.FlatGroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'HWID_REGISTER
        '
        Me.HWID_REGISTER.BackColor = System.Drawing.Color.White
        Me.HWID_REGISTER.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.HWID_REGISTER.BorderColor = System.Drawing.Color.FromArgb(CType(CType(53, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.HWID_REGISTER.Controls.Add(Me.FlatTextBox1)
        Me.HWID_REGISTER.Controls.Add(Me.FlatGroupBox2)
        Me.HWID_REGISTER.Controls.Add(Me.SimplaControlBox1)
        Me.HWID_REGISTER.Controls.Add(Me.FlatButton1)
        Me.HWID_REGISTER.Controls.Add(Me.SECRET_CODE)
        Me.HWID_REGISTER.Controls.Add(Me.FlatGroupBox1)
        Me.HWID_REGISTER.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HWID_REGISTER.FlatColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.HWID_REGISTER.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.HWID_REGISTER.HeaderColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.HWID_REGISTER.HeaderMaximize = False
        Me.HWID_REGISTER.Location = New System.Drawing.Point(0, 0)
        Me.HWID_REGISTER.Name = "HWID_REGISTER"
        Me.HWID_REGISTER.Size = New System.Drawing.Size(450, 412)
        Me.HWID_REGISTER.TabIndex = 0
        Me.HWID_REGISTER.TabStop = False
        Me.HWID_REGISTER.Text = "                                R E G I S T E R"
        '
        'FlatTextBox1
        '
        Me.FlatTextBox1.BackColor = System.Drawing.Color.Transparent
        Me.FlatTextBox1.Enabled = False
        Me.FlatTextBox1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatTextBox1.Location = New System.Drawing.Point(-4, 384)
        Me.FlatTextBox1.MaxLength = 32767
        Me.FlatTextBox1.Multiline = False
        Me.FlatTextBox1.Name = "FlatTextBox1"
        Me.FlatTextBox1.ReadOnly = False
        Me.FlatTextBox1.Size = New System.Drawing.Size(459, 29)
        Me.FlatTextBox1.TabIndex = 0
        Me.FlatTextBox1.TabStop = False
        Me.FlatTextBox1.Tag = ""
        Me.FlatTextBox1.Text = "© PoetralesanA - 2018 | Cyber Security Down Team"
        Me.FlatTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.FlatTextBox1.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FlatTextBox1.UseSystemPasswordChar = False
        '
        'FlatGroupBox2
        '
        Me.FlatGroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.FlatGroupBox2.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatGroupBox2.Controls.Add(Me.KEY)
        Me.FlatGroupBox2.Controls.Add(Me.GetRegedit)
        Me.FlatGroupBox2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatGroupBox2.Location = New System.Drawing.Point(3, 134)
        Me.FlatGroupBox2.Name = "FlatGroupBox2"
        Me.FlatGroupBox2.ShowText = True
        Me.FlatGroupBox2.Size = New System.Drawing.Size(447, 169)
        Me.FlatGroupBox2.TabIndex = 0
        Me.FlatGroupBox2.TabStop = False
        Me.FlatGroupBox2.Text = "KEY REGISTER"
        '
        'KEY
        '
        Me.KEY.BackColor = System.Drawing.Color.Transparent
        Me.KEY.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.KEY.Location = New System.Drawing.Point(18, 38)
        Me.KEY.MaxLength = 32767
        Me.KEY.Multiline = True
        Me.KEY.Name = "KEY"
        Me.KEY.ReadOnly = False
        Me.KEY.Size = New System.Drawing.Size(404, 118)
        Me.KEY.TabIndex = 1
        Me.KEY.TabStop = False
        Me.KEY.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.KEY.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.KEY.UseSystemPasswordChar = False
        '
        'GetRegedit
        '
        Me.GetRegedit.BackColor = System.Drawing.Color.Transparent
        Me.GetRegedit.Enabled = False
        Me.GetRegedit.Location = New System.Drawing.Point(237, 38)
        Me.GetRegedit.MaxLength = 32767
        Me.GetRegedit.Multiline = False
        Me.GetRegedit.Name = "GetRegedit"
        Me.GetRegedit.ReadOnly = False
        Me.GetRegedit.Size = New System.Drawing.Size(185, 29)
        Me.GetRegedit.TabIndex = 2
        Me.GetRegedit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.GetRegedit.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GetRegedit.UseSystemPasswordChar = False
        Me.GetRegedit.Visible = False
        '
        'SimplaControlBox1
        '
        Me.SimplaControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.SimplaControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SimplaControlBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.SimplaControlBox1.Location = New System.Drawing.Point(376, 0)
        Me.SimplaControlBox1.Name = "SimplaControlBox1"
        Me.SimplaControlBox1.Size = New System.Drawing.Size(50, 45)
        Me.SimplaControlBox1.TabIndex = 5
        Me.SimplaControlBox1.TabStop = False
        Me.SimplaControlBox1.Text = "SimplaControlBox1"
        '
        'FlatButton1
        '
        Me.FlatButton1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.FlatButton1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton1.Location = New System.Drawing.Point(21, 309)
        Me.FlatButton1.Name = "FlatButton1"
        Me.FlatButton1.Rounded = False
        Me.FlatButton1.Size = New System.Drawing.Size(404, 43)
        Me.FlatButton1.TabIndex = 3
        Me.FlatButton1.TabStop = False
        Me.FlatButton1.Text = "Login"
        Me.FlatButton1.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'SECRET_CODE
        '
        Me.SECRET_CODE.BackColor = System.Drawing.Color.Transparent
        Me.SECRET_CODE.Enabled = False
        Me.SECRET_CODE.Location = New System.Drawing.Point(21, 328)
        Me.SECRET_CODE.MaxLength = 32767
        Me.SECRET_CODE.Multiline = False
        Me.SECRET_CODE.Name = "SECRET_CODE"
        Me.SECRET_CODE.ReadOnly = False
        Me.SECRET_CODE.Size = New System.Drawing.Size(405, 29)
        Me.SECRET_CODE.TabIndex = 0
        Me.SECRET_CODE.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.SECRET_CODE.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.SECRET_CODE.UseSystemPasswordChar = False
        '
        'FlatGroupBox1
        '
        Me.FlatGroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.FlatGroupBox1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatGroupBox1.Controls.Add(Me.HWID)
        Me.FlatGroupBox1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatGroupBox1.Location = New System.Drawing.Point(1, 63)
        Me.FlatGroupBox1.Name = "FlatGroupBox1"
        Me.FlatGroupBox1.ShowText = True
        Me.FlatGroupBox1.Size = New System.Drawing.Size(447, 93)
        Me.FlatGroupBox1.TabIndex = 0
        Me.FlatGroupBox1.TabStop = False
        Me.FlatGroupBox1.Text = "HWID"
        '
        'HWID
        '
        Me.HWID.BackColor = System.Drawing.Color.Transparent
        Me.HWID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.HWID.Location = New System.Drawing.Point(18, 36)
        Me.HWID.MaxLength = 32767
        Me.HWID.Multiline = False
        Me.HWID.Name = "HWID"
        Me.HWID.ReadOnly = True
        Me.HWID.Size = New System.Drawing.Size(406, 29)
        Me.HWID.TabIndex = 0
        Me.HWID.TabStop = False
        Me.HWID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.HWID.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.HWID.UseSystemPasswordChar = False
        '
        'RegisterForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 412)
        Me.Controls.Add(Me.HWID_REGISTER)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "RegisterForm"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "HWID REGISTER"
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.HWID_REGISTER.ResumeLayout(False)
        Me.FlatGroupBox2.ResumeLayout(False)
        Me.FlatGroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlatButton1 As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatButton
    Friend WithEvents FlatGroupBox2 As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatGroupBox
    Friend WithEvents KEY As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox
    Friend WithEvents HWID_REGISTER As WIFI.ID_FVKIN666_EVOLUTION_V3.FormSkin
    Friend WithEvents SECRET_CODE As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox
    Friend WithEvents SimplaControlBox1 As WIFI.ID_FVKIN666_EVOLUTION_V3.SimplaControlBox
    Friend WithEvents FlatTextBox1 As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox
    Friend WithEvents GetRegedit As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox
    Friend WithEvents FlatGroupBox1 As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatGroupBox
    Friend WithEvents HWID As WIFI.ID_FVKIN666_EVOLUTION_V3.FlatTextBox
End Class
